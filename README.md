## Installation

```bash
$ npm install -g ts-node
$ npm install
```

## Build project

```bash
$ npm install -g tsc
$ npm run build
```

## Running the app

```bash
# development
$ npm run dev

# production mode
$ npm run start
```

## Database

```bash
dev_web
```

## How to run in development

```bash
# 0. install xampp
# 1. import dev_web.sql to database dev_web
# 2. project installation
$ npm install -g ts-node
$ npm install -g tsc
$ npm install
# 3. run project
$ npm run dev
```
