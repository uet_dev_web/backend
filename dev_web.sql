-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 24, 2022 at 05:15 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dev_web`
--

-- --------------------------------------------------------

--
-- Table structure for table `chung_nhan`
--

CREATE TABLE `chung_nhan` (
  `id` int(10) UNSIGNED NOT NULL,
  `ngayCap` datetime DEFAULT NULL,
  `ngayHetHan` datetime NOT NULL,
  `hieuLuc` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chung_nhan`
--

INSERT INTO `chung_nhan` (`id`, `ngayCap`, `ngayHetHan`, `hieuLuc`) VALUES
(1, '2022-04-30 00:00:00', '2022-06-19 00:00:00', 1),
(2, '2022-04-28 00:00:00', '2022-06-17 00:00:00', 1),
(3, '2022-04-28 00:00:00', '2022-06-17 00:00:00', 1),
(4, '2022-04-28 00:00:00', '2022-06-17 00:00:00', 1),
(5, '2022-04-21 00:00:00', '2022-06-10 00:00:00', 1),
(6, '2022-04-29 00:00:00', '2022-06-18 00:00:00', 1),
(7, '2022-04-25 00:00:00', '2022-06-14 00:00:00', 1),
(8, '2022-04-28 00:00:00', '2022-06-17 00:00:00', 1),
(9, '2022-04-28 00:00:00', '2022-06-17 00:00:00', 1),
(10, '2022-04-24 00:00:00', '2022-06-13 00:00:00', 1),
(11, '2022-04-29 00:00:00', '2022-06-18 00:00:00', 1),
(12, '2022-04-21 00:00:00', '2022-06-10 00:00:00', 1),
(13, '2022-04-23 00:00:00', '2022-06-12 00:00:00', 1),
(14, '2022-04-25 00:00:00', '2022-06-14 00:00:00', 1),
(15, '2022-04-22 00:00:00', '2022-06-11 00:00:00', 1),
(16, '2022-04-25 00:00:00', '2022-06-14 00:00:00', 1),
(17, '2022-04-21 00:00:00', '2022-06-10 00:00:00', 1),
(18, '2022-04-21 00:00:00', '2022-06-10 00:00:00', 1),
(19, '2022-04-23 00:00:00', '2022-06-12 00:00:00', 1),
(20, '2022-04-28 00:00:00', '2022-06-17 00:00:00', 1),
(21, '2022-04-24 00:00:00', '2022-06-13 00:00:00', 1),
(22, '2022-04-21 00:00:00', '2022-06-10 00:00:00', 1),
(23, '2022-04-29 00:00:00', '2022-06-18 00:00:00', 1),
(24, '2022-04-30 00:00:00', '2022-06-19 00:00:00', 1),
(25, '2022-04-22 00:00:00', '2022-06-11 00:00:00', 1),
(26, '2022-04-30 00:00:00', '2022-06-19 00:00:00', 1),
(27, '2022-04-24 00:00:00', '2022-06-13 00:00:00', 1),
(28, '2022-05-24 14:59:28', '2022-10-08 17:00:00', 1),
(29, '2022-05-24 15:00:00', '2022-10-08 17:00:00', 1),
(30, '2022-05-24 15:03:46', '2022-10-08 17:00:00', 1),
(31, '2022-05-24 15:04:08', '2022-10-08 17:00:00', 1),
(32, '2022-05-24 15:05:54', '2022-10-08 17:00:00', 1),
(33, '2022-05-24 15:09:08', '2022-05-24 15:09:08', 1),
(34, '2022-05-24 15:13:22', '2022-10-08 17:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `co_so`
--

CREATE TABLE `co_so` (
  `id` int(10) UNSIGNED NOT NULL,
  `ten` varchar(255) NOT NULL,
  `diaChi` text DEFAULT NULL,
  `sdt` varchar(20) DEFAULT NULL,
  `loaiHinh` varchar(255) DEFAULT NULL,
  `huyenQuanId` varchar(255) DEFAULT NULL,
  `xaPhuongId` varchar(255) DEFAULT NULL,
  `chungNhanId` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `co_so`
--

INSERT INTO `co_so` (`id`, `ten`, `diaChi`, `sdt`, `loaiHinh`, `huyenQuanId`, `xaPhuongId`, `chungNhanId`) VALUES
(1, 'Bún đậu ông hachi', 'Số 1 Ngách 23/34 Phạm Văn Đồng', '0983154343', 'Sản xuất', '02', '0202', 1),
(2, 'Gà rán ông nana', 'Số 5 Ngách 44/11 Nguyễn Hoàng', '0986475656', 'Sản xuất', '09', '0901', 2),
(3, 'Phở bà yon', 'Số 5 Ngách 23/34 Phạm Văn Đồng', '0989240682', 'Sản xuất và dịch vụ', '03', '0302', 3),
(4, 'Ốc bà jyu', 'Số 5 Ngách 63/33 Nguyễn Hoàng', '0984559257', 'Dịch vụ', '03', '0301', 4),
(5, 'Lẩu ông ni', 'Số 1 Ngách 44/11 Nguyễn Hoàng', '0980420443', 'Dịch vụ', '09', '0908', 34),
(6, 'Buffet bà kyu', 'Số 4 Ngách 52/23 Trần Duy Hưng', '0982498216', 'Dịch vụ', '09', '0904', 6),
(7, 'Ốc bà go', 'Số 3 Ngách 12/22 Phạm Văn Đồng', '0985072321', 'Dịch vụ', '02', '0204', 7),
(8, 'Cơm ông yon', 'Số 3 Ngách 23/34 Xuân Thủy', '0984284810', 'Sản xuất và dịch vụ', '05', '0503', 8),
(9, 'Buffet ông go', 'Số 5 Ngách 63/33 Nguyễn Hoàng', '0983478176', 'Sản xuất', '02', '0203', 9),
(10, 'Lẩu ông jyu', 'Số 1 Ngách 52/23 Trần Duy Hưng', '0980810654', 'Sản xuất', '03', '0301', 10),
(11, 'Phở bà ichi', 'Số 1 Ngách 63/33 Xuân Thủy', '0982851290', 'Sản xuất và dịch vụ', '05', '0508', 11),
(12, 'Ốc ông nana', 'Số 1 Ngách 12/22 Xuân Thủy', '0989225838', 'Dịch vụ', '02', '0209', 12),
(13, 'Lẩu bà yon', 'Số 3 Ngách 52/23 Phạm Văn Đồng', '0986493996', 'Sản xuất và dịch vụ', '01', '0104', 13),
(14, 'Bia bà ni', 'Số 2 Ngách 44/11 Nguyễn Hoàng', '0989221047', 'Sản xuất', '06', '0602', 14),
(15, 'Bia bà san', 'Số 4 Ngách 23/34 Trần Duy Hưng', '0982143108', 'Dịch vụ', '09', '0904', 15),
(16, 'Chè bà go', 'Số 3 Ngách 52/23 Phạm Văn Đồng', '0981650406', 'Dịch vụ', '01', '0103', 16),
(17, 'Buffet bà jyu', 'Số 4 Ngách 23/34 Xuân Thủy', '0983573311', 'Sản xuất và dịch vụ', '06', '0602', 17),
(18, 'Bò nướng bà roku', 'Số 1 Ngách 12/22 Xuân Thủy', '0989979762', 'Dịch vụ', '05', '0502', 18),
(19, 'Lẩu ông ni', 'Số 1 Ngách 23/34 Nguyễn Hoàng', '0988615502', 'Sản xuất', '01', '0106', 19),
(20, 'Lẩu bà go', 'Số 3 Ngách 44/11 Nguyễn Hoàng', '0982740044', 'Sản xuất', '08', '0803', 20),
(21, 'Ốc bà yon', 'Số 2 Ngách 12/22 Hồ Tùng Mậu', '0985626494', 'Sản xuất', '01', '0101', 21),
(22, 'Bò nướng bà kyu', 'Số 1 Ngách 23/34 Hồ Tùng Mậu', '0980806311', 'Dịch vụ', '01', '0107', 22),
(23, 'Ốc ông ni', 'Số 5 Ngách 44/11 Xuân Thủy', '0988719779', 'Sản xuất và dịch vụ', '09', '0904', 23),
(24, 'Cơm bà san', 'Số 2 Ngách 44/11 Trần Duy Hưng', '0985186114', 'Dịch vụ', '04', '0408', 24),
(25, 'Bia ông jyu', 'Số 2 Ngách 52/23 Hồ Tùng Mậu', '0984591952', 'Sản xuất và dịch vụ', '03', '0306', 25),
(26, 'Bia ông nana', 'Số 5 Ngách 23/34 Phạm Văn Đồng', '0986357470', 'Sản xuất', '02', '0209', 26),
(27, 'Bún đậu ông san', 'Số 1 Ngách 23/34 Phạm Văn Đồng', '0985862796', 'Sản xuất và dịch vụ', '07', '0701', 27);

-- --------------------------------------------------------

--
-- Table structure for table `huyen_quan`
--

CREATE TABLE `huyen_quan` (
  `ten` varchar(255) NOT NULL,
  `id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `huyen_quan`
--

INSERT INTO `huyen_quan` (`ten`, `id`) VALUES
('Quận Ba Đình', '01'),
('Quận Hoàn Kiếm', '02'),
('Quận Tây Hồ', '03'),
('Quận Long Biên', '04'),
('Quận Cầu Giấy', '05'),
('Quận Đống Đa', '06'),
('Quận Hai Bà Trưng', '07'),
('Quận Hoàng Mai', '08'),
('Quận Thanh Xuân', '09'),
('Huyện Sóc Sơn', '10'),
('Huyện Đông Anh', '11'),
('Huyện Gia Lâm', '12'),
('Quận Nam Từ Liêm', '13'),
('Huyện Thanh Trì', '14'),
('Quận Bắc Từ Liêm', '15'),
('Huyện Mê Linh', '16'),
('Quận Hà Đông', '17'),
('Thị xã Sơn Tây', '18'),
('Huyện Ba Vì', '19'),
('Huyện Phúc Thọ', '20'),
('Huyện Đan Phượng', '21'),
('Huyện Hoài Đức', '22'),
('Huyện Quốc Oai', '23'),
('Huyện Thạch Thất', '24'),
('Huyện Chương Mỹ', '25'),
('Huyện Thanh Oai', '26'),
('Huyện Thường Tín', '27'),
('Huyện Phú Xuyên', '28'),
('Huyện Ứng Hòa', '29'),
('Huyện Mỹ Đức', '30');

-- --------------------------------------------------------

--
-- Table structure for table `mau_vat`
--

CREATE TABLE `mau_vat` (
  `id` int(11) NOT NULL,
  `anToan` tinyint(4) NOT NULL DEFAULT 0,
  `thanhTraId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mau_vat`
--

INSERT INTO `mau_vat` (`id`, `anToan`, `thanhTraId`) VALUES
(1, 0, 1),
(2, 1, 1),
(3, 0, 2),
(4, 1, 2),
(5, 1, 3),
(6, 0, 3),
(7, 0, 4),
(8, 1, 5),
(9, 0, 5),
(10, 0, 6);

-- --------------------------------------------------------

--
-- Table structure for table `nguoi_dung`
--

CREATE TABLE `nguoi_dung` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `diaBanId` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `nguoi_dung`
--

INSERT INTO `nguoi_dung` (`username`, `password`, `diaBanId`) VALUES
('chuyenvien01', 'chuyenvien01', '01'),
('chuyenvien02', 'chuyenvien02', '02'),
('chuyenvien03', 'chuyenvien03', '03'),
('chuyenvien04', 'chuyenvien04', '04'),
('chuyenvien05', 'chuyenvien05', '05'),
('chuyenvien06', 'chuyenvien06', '06'),
('chuyenvien07', 'chuyenvien07', '07'),
('chuyenvien08', 'chuyenvien08', '08'),
('chuyenvien09', 'chuyenvien09', '09'),
('quanly01', 'quanly01', NULL),
('quanly02', 'quanly02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `thanh_tra`
--

CREATE TABLE `thanh_tra` (
  `id` int(11) NOT NULL,
  `ngayBatDau` datetime DEFAULT NULL,
  `ngayKetThuc` datetime DEFAULT NULL,
  `coSoId` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `thanh_tra`
--

INSERT INTO `thanh_tra` (`id`, `ngayBatDau`, `ngayKetThuc`, `coSoId`) VALUES
(1, '2022-04-08 00:00:00', '2022-04-25 00:00:00', 1),
(2, '2022-04-11 00:00:00', '2022-04-23 00:00:00', 2),
(3, '2022-04-12 00:00:00', '2022-04-25 00:00:00', 3),
(4, '2022-04-10 00:00:00', '2022-04-24 00:00:00', 4),
(5, '2022-04-15 00:00:00', '2022-04-29 00:00:00', 5),
(6, '2022-04-15 00:00:00', '2022-04-23 00:00:00', 6),
(7, '2022-04-19 00:00:00', '2022-04-25 00:00:00', 7),
(8, '2022-04-18 00:00:00', '2022-04-22 00:00:00', 8),
(9, '2022-04-09 00:00:00', '2022-04-27 00:00:00', 9),
(10, '2022-04-20 00:00:00', '2022-04-26 00:00:00', 10);

-- --------------------------------------------------------

--
-- Table structure for table `xa_phuong`
--

CREATE TABLE `xa_phuong` (
  `ten` varchar(255) NOT NULL,
  `huyenQuanId` varchar(255) DEFAULT NULL,
  `id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `xa_phuong`
--

INSERT INTO `xa_phuong` (`ten`, `huyenQuanId`, `id`) VALUES
('Phường Phúc Xá', '01', '0101'),
('Phường Trúc Bạch', '01', '0102'),
('Phường Vĩnh Phúc', '01', '0103'),
('Phường Cống Vị', '01', '0104'),
('Phường Liễu Giai', '01', '0105'),
('Phường Nguyễn Trung Trực', '01', '0106'),
('Phường Quán Thánh', '01', '0107'),
('Phường Ngọc Hà', '01', '0108'),
('Phường Điện Biên', '01', '0109'),
('Phường Đội Cấn', '01', '0110'),
('Phường Ngọc Khánh', '01', '0111'),
('Phường Kim Mã', '01', '0112'),
('Phường Giảng Võ', '01', '0113'),
('Phường Thành Công', '01', '0114'),
('Phường Phúc Tân', '02', '0201'),
('Phường Đồng Xuân', '02', '0202'),
('Phường Hàng Mã', '02', '0203'),
('Phường Hàng Buồm', '02', '0204'),
('Phường Hàng Đào', '02', '0205'),
('Phường Hàng Bồ', '02', '0206'),
('Phường Cửa Đông', '02', '0207'),
('Phường Lý Thái Tổ', '02', '0208'),
('Phường Hàng Bạc', '02', '0209'),
('Phường Hàng Gai', '02', '0210'),
('Phường Chương Dương', '02', '0211'),
('Phường Hàng Trống', '02', '0212'),
('Phường Cửa Nam', '02', '0213'),
('Phường Hàng Bông', '02', '0214'),
('Phường Tràng Tiền', '02', '0215'),
('Phường Trần Hưng Đạo', '02', '0216'),
('Phường Phan Chu Trinh', '02', '0217'),
('Phường Hàng Bài', '02', '0218'),
('Phường Phú Thượng', '03', '0301'),
('Phường Nhật Tân', '03', '0302'),
('Phường Tứ Liên', '03', '0303'),
('Phường Quảng An', '03', '0304'),
('Phường Xuân La', '03', '0305'),
('Phường Yên Phụ', '03', '0306'),
('Phường Bưởi', '03', '0307'),
('Phường Thụy Khuê', '03', '0308'),
('Phường Thượng Thanh', '04', '0401'),
('Phường Ngọc Thụy', '04', '0402'),
('Phường Giang Biên', '04', '0403'),
('Phường Đức Giang', '04', '0404'),
('Phường Việt Hưng', '04', '0405'),
('Phường Gia Thụy', '04', '0406'),
('Phường Ngọc Lâm', '04', '0407'),
('Phường Phúc Lợi', '04', '0408'),
('Phường Bồ Đề', '04', '0409'),
('Phường Sài Đồng', '04', '0410'),
('Phường Long Biên', '04', '0411'),
('Phường Thạch Bàn', '04', '0412'),
('Phường Phúc Đồng', '04', '0413'),
('Phường Cự Khối', '04', '0414'),
('Phường Nghĩa Đô', '05', '0501'),
('Phường Nghĩa Tân', '05', '0502'),
('Phường Mai Dịch', '05', '0503'),
('Phường Dịch Vọng', '05', '0504'),
('Phường Dịch Vọng Hậu', '05', '0505'),
('Phường Quan Hoa', '05', '0506'),
('Phường Yên Hoà', '05', '0507'),
('Phường Trung Hoà', '05', '0508'),
('Phường Cát Linh', '06', '0601'),
('Phường Văn Miếu', '06', '0602'),
('Phường Quốc Tử Giám', '06', '0603'),
('Phường Láng Thượng', '06', '0604'),
('Phường Ô Chợ Dừa', '06', '0605'),
('Phường Văn Chương', '06', '0606'),
('Phường Hàng Bột', '06', '0607'),
('Phường Láng Hạ', '06', '0608'),
('Phường Khâm Thiên', '06', '0609'),
('Phường Thổ Quan', '06', '0610'),
('Phường Nam Đồng', '06', '0611'),
('Phường Trung Phụng', '06', '0612'),
('Phường Quang Trung', '06', '0613'),
('Phường Trung Liệt', '06', '0614'),
('Phường Phương Liên', '06', '0615'),
('Phường Thịnh Quang', '06', '0616'),
('Phường Trung Tự', '06', '0617'),
('Phường Kim Liên', '06', '0618'),
('Phường Phương Mai', '06', '0619'),
('Phường Ngã Tư Sở', '06', '0620'),
('Phường Khương Thượng', '06', '0621'),
('Phường Nguyễn Du', '07', '0701'),
('Phường Bạch Đằng', '07', '0702'),
('Phường Phạm Đình Hổ', '07', '0703'),
('Phường Lê Đại Hành', '07', '0704'),
('Phường Đồng Nhân', '07', '0705'),
('Phường Phố Huế', '07', '0706'),
('Phường Đống Mác', '07', '0707'),
('Phường Thanh Lương', '07', '0708'),
('Phường Thanh Nhàn', '07', '0709'),
('Phường Cầu Dền', '07', '0710'),
('Phường Bách Khoa', '07', '0711'),
('Phường Đồng Tâm', '07', '0712'),
('Phường Vĩnh Tuy', '07', '0713'),
('Phường Bạch Mai', '07', '0714'),
('Phường Quỳnh Mai', '07', '0715'),
('Phường Quỳnh Lôi', '07', '0716'),
('Phường Minh Khai', '07', '0717'),
('Phường Trương Định', '07', '0718'),
('Phường Thanh Trì', '08', '0801'),
('Phường Vĩnh Hưng', '08', '0802'),
('Phường Định Công', '08', '0803'),
('Phường Mai Động', '08', '0804'),
('Phường Tương Mai', '08', '0805'),
('Phường Đại Kim', '08', '0806'),
('Phường Tân Mai', '08', '0807'),
('Phường Hoàng Văn Thụ', '08', '0808'),
('Phường Giáp Bát', '08', '0809'),
('Phường Lĩnh Nam', '08', '0810'),
('Phường Thịnh Liệt', '08', '0811'),
('Phường Trần Phú', '08', '0812'),
('Phường Hoàng Liệt', '08', '0813'),
('Phường Yên Sở', '08', '0814'),
('Phường Nhân Chính', '09', '0901'),
('Phường Thượng Đình', '09', '0902'),
('Phường Khương Trung', '09', '0903'),
('Phường Khương Mai', '09', '0904'),
('Phường Thanh Xuân Trung', '09', '0905'),
('Phường Phương Liệt', '09', '0906'),
('Phường Hạ Đình', '09', '0907'),
('Phường Khương Đình', '09', '0908'),
('Phường Thanh Xuân Bắc', '09', '0909'),
('Phường Thanh Xuân Nam', '09', '0910'),
('Phường Kim Giang', '09', '0911'),
('Thị trấn Sóc Sơn', '10', '1001'),
('Xã Bắc Sơn', '10', '1002'),
('Xã Minh Trí', '10', '1003'),
('Xã Hồng Kỳ', '10', '1004'),
('Xã Nam Sơn', '10', '1005'),
('Xã Trung Giã', '10', '1006'),
('Xã Tân Hưng', '10', '1007'),
('Xã Minh Phú', '10', '1008'),
('Xã Phù Linh', '10', '1009'),
('Xã Bắc Phú', '10', '1010'),
('Xã Tân Minh', '10', '1011'),
('Xã Quang Tiến', '10', '1012'),
('Xã Hiền Ninh', '10', '1013'),
('Xã Tân Dân', '10', '1014'),
('Xã Tiên Dược', '10', '1015'),
('Xã Việt Long', '10', '1016'),
('Xã Xuân Giang', '10', '1017'),
('Xã Mai Đình', '10', '1018'),
('Xã Đức Hoà', '10', '1019'),
('Xã Thanh Xuân', '10', '1020'),
('Xã Đông Xuân', '10', '1021'),
('Xã Kim Lũ', '10', '1022'),
('Xã Phú Cường', '10', '1023'),
('Xã Phú Minh', '10', '1024'),
('Xã Phù Lỗ', '10', '1025'),
('Xã Xuân Thu', '10', '1026');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chung_nhan`
--
ALTER TABLE `chung_nhan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `co_so`
--
ALTER TABLE `co_so`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `REL_a563a202270b44063ffa1c9eca` (`chungNhanId`),
  ADD KEY `FK_cc16177844925cdfcb08bca6390` (`huyenQuanId`),
  ADD KEY `FK_cf7699d43b57c296b5249b4ba4f` (`xaPhuongId`);

--
-- Indexes for table `huyen_quan`
--
ALTER TABLE `huyen_quan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mau_vat`
--
ALTER TABLE `mau_vat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_b623cf8a7de3f8c2a67b376d900` (`thanhTraId`);

--
-- Indexes for table `nguoi_dung`
--
ALTER TABLE `nguoi_dung`
  ADD PRIMARY KEY (`username`),
  ADD KEY `FK_ea12a0a33910ae471423fd5814b` (`diaBanId`);

--
-- Indexes for table `thanh_tra`
--
ALTER TABLE `thanh_tra`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_b88a4eafbcf01c99bb0070bfdcb` (`coSoId`);

--
-- Indexes for table `xa_phuong`
--
ALTER TABLE `xa_phuong`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_678e936a79a6973126ce02d841f` (`huyenQuanId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chung_nhan`
--
ALTER TABLE `chung_nhan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `co_so`
--
ALTER TABLE `co_so`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `mau_vat`
--
ALTER TABLE `mau_vat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `thanh_tra`
--
ALTER TABLE `thanh_tra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `co_so`
--
ALTER TABLE `co_so`
  ADD CONSTRAINT `FK_a563a202270b44063ffa1c9eca8` FOREIGN KEY (`chungNhanId`) REFERENCES `chung_nhan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_cc16177844925cdfcb08bca6390` FOREIGN KEY (`huyenQuanId`) REFERENCES `huyen_quan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_cf7699d43b57c296b5249b4ba4f` FOREIGN KEY (`xaPhuongId`) REFERENCES `xa_phuong` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mau_vat`
--
ALTER TABLE `mau_vat`
  ADD CONSTRAINT `FK_b623cf8a7de3f8c2a67b376d900` FOREIGN KEY (`thanhTraId`) REFERENCES `thanh_tra` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `nguoi_dung`
--
ALTER TABLE `nguoi_dung`
  ADD CONSTRAINT `FK_ea12a0a33910ae471423fd5814b` FOREIGN KEY (`diaBanId`) REFERENCES `huyen_quan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `thanh_tra`
--
ALTER TABLE `thanh_tra`
  ADD CONSTRAINT `FK_b88a4eafbcf01c99bb0070bfdcb` FOREIGN KEY (`coSoId`) REFERENCES `co_so` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `xa_phuong`
--
ALTER TABLE `xa_phuong`
  ADD CONSTRAINT `FK_678e936a79a6973126ce02d841f` FOREIGN KEY (`huyenQuanId`) REFERENCES `huyen_quan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
