import { Router } from 'express'
import xaPhuongController from '../../controllers/xaPhuong.controller'

const router = Router()

router.get('/', xaPhuongController.getXaPhuongs)
// router.get('/columns', xaPhuongController.getColumns)
router.get('/:id', xaPhuongController.getXaPhuong)
router.post('/', xaPhuongController.postXaPhuong)
router.put('/:id', xaPhuongController.putXaPhuong)
router.delete('/:id', xaPhuongController.deleteXaPhuong)

export default router
