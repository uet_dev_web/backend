import { Router } from 'express'
import huyenQuanController from '../../controllers/huyenQuan.controller'

const router = Router()

router.get('/', huyenQuanController.getHuyenQuans)
// router.get('/columns', huyenQuanController.getColumns)
router.get('/:id', huyenQuanController.getHuyenQuan)
router.post('/', huyenQuanController.postHuyenQuan)
router.put('/:id', huyenQuanController.putHuyenQuan)
router.delete('/:id', huyenQuanController.deleteHuyenQuan)

export default router
