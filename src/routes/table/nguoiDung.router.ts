import { Router } from 'express'
import nguoiDungController from '../../controllers/nguoiDung.controller'

const router = Router()

router.get('/', nguoiDungController.getNguoiDungs)
// router.get('/columns', nguoiDungController.getColumns)
router.get('/info', nguoiDungController.getSelfInfo)
router.get('/:username', nguoiDungController.getNguoiDung)
router.post('/', nguoiDungController.postNguoiDung)
router.put('/:username', nguoiDungController.putNguoiDung)
router.delete('/:username', nguoiDungController.deleteNguoiDung)

export default router
