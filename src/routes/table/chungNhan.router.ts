import { Router } from 'express'
import chungNhanController from '../../controllers/chungNhan.controller'

const router = Router()

router.get('/', chungNhanController.getChungNhans)
// router.get('/columns', chungNhanController.getColumns)
router.get('/:id', chungNhanController.getChungNhan)
router.post('/', chungNhanController.postChungNhan)
router.put('/:id', chungNhanController.putChungNhan)
router.delete('/:id', chungNhanController.deleteChungNhan)

export default router
