import { Router } from 'express'
import thanhTraController from '../../controllers/thanhTra.controller'

const router = Router()

router.get('/', thanhTraController.getThanhTras)
// router.get('/columns', thanhTraController.getColumns)
router.get('/:id', thanhTraController.getThanhTra)
router.post('/', thanhTraController.postThanhTra)
router.put('/:id', thanhTraController.putThanhTra)
router.delete('/:id', thanhTraController.deleteThanhTra)

export default router
