import { Router } from 'express'
import coSoController from '../../controllers/coSo.controller'

const router = Router()

router.get('/', coSoController.getCoSos)
// router.get('/columns', coSoController.getColumns)
router.get('/:id', coSoController.getCoSo)
router.post('/', coSoController.postCoSo)
router.put('/:id', coSoController.putCoSo)
router.delete('/:id', coSoController.deleteCoSo)

export default router
