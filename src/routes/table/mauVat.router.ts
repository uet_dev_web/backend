import { Router } from 'express'
import mauVatController from '../../controllers/mauVat.controller'

const router = Router()

router.get('/', mauVatController.getMauVats)
// router.get('/columns', mauVatController.getColumns)
router.get('/:id', mauVatController.getMauVat)
router.post('/', mauVatController.postMauVat)
router.put('/:id', mauVatController.putMauVat)
router.delete('/:id', mauVatController.deleteMauVat)

export default router
