import authController from '../controllers/auth.controller'
import sideController from '../controllers/side'
import auth from '../middlewares/auth'
import apiV1Router from './api.v1.router'

export default function routes(app) {
  // docx
  app.get('/', sideController.getSwaggerFile)
  app.post('/login', authController.login)

  // need login
  app.use('/', auth.checkToken)
  app.post('/logout', authController.logout)
  app.use('/', apiV1Router)

  // catch 404
  app.use('/', sideController.catch404)
}
