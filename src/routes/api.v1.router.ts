import { Router } from 'express'
import chungNhanRouter from './table/chungNhan.router'
import coSoRouter from './table/coSo.router'
import huyenQuanRouter from './table/huyenQuan.router'
import mauVatRouter from './table/mauVat.router'
import nguoiDungRouter from './table/nguoiDung.router'
import thanhTraRouter from './table/thanhTra.router'
import xaPhuongRouter from './table/xaPhuong.router'
const router = Router()

// chungNhan api
router.use('/chungNhan', chungNhanRouter)

// coSo api
router.use('/coSo', coSoRouter)

// huyenQuan api
router.use('/huyenQuan', huyenQuanRouter)

// mauVat api
router.use('/mauVat', mauVatRouter)

// nguoiDung api
router.use('/nguoiDung', nguoiDungRouter)

// thanhTra api
router.use('/thanhTra', thanhTraRouter)

// xaPhuong api
router.use('/xaPhuong', xaPhuongRouter)

export default router
