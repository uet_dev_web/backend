import { Request, Response } from 'express'
import { MauVat } from '../database/entity/MauVat.entity'
import AppDataSource from '../database/connection'
import helper from './helper'
import { ThanhTra } from '../database/entity/ThanhTra.entity'

export default {
  // [GET] /mauVat/?page=1
  getMauVats: async function (req: Request, res: Response) {
    const pageNumber = Number(req.query.page) || 1
    const pageSize = Number(req.query.pageSize) || 10
    const mauVatRepository = AppDataSource.getRepository(MauVat)
    const result = await helper.paginationDoubleJoin(
      mauVatRepository,
      'thanhTra',
      pageNumber,
      pageSize
    )
    res.json(result)
  },

  // [GET] /mauVat/:id
  getMauVat: async function (req: Request, res: Response) {
    const id = Number(req.params.id)
    const mauVatRepository = AppDataSource.getRepository(MauVat)
    const result = await helper.getAmountDoubleJoin(
      mauVatRepository,
      'thanhTra',
      'getOne',
      `alias.id = ${id}`
    )
    res.json(result)
  },

  // [POST] /mauVat
  postMauVat: async function (req: Request, res: Response) {
    const id = String(req.body.id)
    const mauVatRepository = AppDataSource.getRepository(MauVat)
    const thanhTraRepository = AppDataSource.getRepository(ThanhTra)

    try {
      const mauVat = new MauVat()
      const thanhTraId = Number(req.body.thanhTraId)
      if (thanhTraId)
        mauVat.thanhTra = await helper.checkFindOne(
          res,
          thanhTraRepository,
          { id: thanhTraId },
          ['thanhTraId is invalid', 'thanhTraId must be an integer number']
        )
      mauVat.anToan = Boolean(req.body.anToan)

      // validate
      await helper.validate(res, mauVat)
      const result = await mauVatRepository.save(mauVat)
      res.json({ msg: 'Insert complete', result: JSON.stringify(result) })
    } catch (error) {}
  },

  // [PUT] /mauVat/:id
  putMauVat: async function (req: Request, res: Response) {
    const id = Number(req.params.id)
    const mauVatRepository = AppDataSource.getRepository(MauVat)
    const thanhTraRepository = AppDataSource.getRepository(ThanhTra)

    try {
      // check valid id
      const thanhTraId = Number(req.body.thanhTraId)
      const mauVat = await helper.checkFindOne(
        res,
        mauVatRepository,
        { id: id },
        ['id is invalid', 'id must be a string']
      )
      if (thanhTraId)
        mauVat.thanhTra = await helper.checkFindOne(
          res,
          thanhTraRepository,
          { id: thanhTraId },
          ['thanhTraId is invalid', 'thanhTraId must be an integer number']
        )

      mauVat.anToan = Boolean(req.body.anToan)

      // validate
      await helper.validate(res, mauVat)
      const result = await mauVatRepository.save(mauVat)
      res.json({ msg: 'Update complete', result: JSON.stringify(result) })
    } catch (error) {}
  },

  // [DELETE] /mauVat/:id
  deleteMauVat: async function (req: Request, res: Response) {
    const id = Number(req.params.id)
    const mauVatRepository = AppDataSource.getRepository(MauVat)

    try {
      const mauVat = await mauVatRepository.findOneBy({ id: id })
      const result = await mauVatRepository.remove(mauVat)
      res.json({ msg: 'Delete complete', result: JSON.stringify(result) })
    } catch (error) {
      res.json({ msg: 'Invalid id' })
    }
  },
}
