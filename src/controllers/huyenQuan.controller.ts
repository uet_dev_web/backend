import { Request, Response } from 'express'
import { HuyenQuan } from '../database/entity/HuyenQuan.entity'
import AppDataSource from '../database/connection'
import helper from './helper'

export default {
  // [GET] /huyenQuan/?page=1
  getHuyenQuans: async function (req: Request, res: Response) {
    const pageNumber = Number(req.query.page) || 1
    const pageSize = Number(req.query.pageSize) || 10
    const huyenQuanRepository = AppDataSource.getRepository(HuyenQuan)
    const result = await helper.pagination(
      huyenQuanRepository,
      pageNumber,
      pageSize
    )
    res.json(result)
  },

  // [GET] /huyenQuan/:id
  getHuyenQuan: async function (req: Request, res: Response) {
    const id = Number(req.params.id)
    const huyenQuanRepository = AppDataSource.getRepository(HuyenQuan)
    const result = await helper.getAmountDoubleJoin(
      huyenQuanRepository,
      'xaPhuongs',
      'getOne',
      `alias.id = ${id}`
    )
    res.json(result)
  },

  // [POST] /huyenQuan
  postHuyenQuan: async function (req: Request, res: Response) {
    const id = String(req.body.id)
    const huyenQuanRepository = AppDataSource.getRepository(HuyenQuan)

    try {
      const huyenQuan = new HuyenQuan()
      huyenQuan.ten = String(req.body.ten)
      huyenQuan.id = id

      // validate
      await helper.validate(res, huyenQuan)
      const result = await huyenQuanRepository.save(huyenQuan)
      res.json({ msg: 'Insert complete', result: JSON.stringify(result) })
    } catch (error) {}
  },

  // [PUT] /huyenQuan/:id
  putHuyenQuan: async function (req: Request, res: Response) {
    const id = String(req.params.id)
    const huyenQuanRepository = AppDataSource.getRepository(HuyenQuan)

    try {
      // check valid id
      const huyenQuan = await helper.checkFindOne(
        res,
        huyenQuanRepository,
        { id: id },
        ['id is invalid', 'id must be a string']
      )
      huyenQuan.ten = String(req.body.ten)

      // validate
      await helper.validate(res, huyenQuan)
      const result = await huyenQuanRepository.save(huyenQuan)
      res.json({ msg: 'Update complete', result: JSON.stringify(result) })
    } catch (error) {}
  },

  // [DELETE] /huyenQuan/:id
  deleteHuyenQuan: async function (req: Request, res: Response) {
    const id = String(req.params.id)
    const huyenQuanRepository = AppDataSource.getRepository(HuyenQuan)

    try {
      const huyenQuan = await huyenQuanRepository.findOneBy({ id: id })
      const result = await huyenQuanRepository.remove(huyenQuan)
      res.json({ msg: 'Delete complete', result: JSON.stringify(result) })
    } catch (error) {
      res.json({ msg: 'Invalid id or having foreign key' })
    }
  },
}
