import { Request, Response } from 'express'
import { XaPhuong } from '../database/entity/XaPhuong.entity'
import AppDataSource from '../database/connection'
import helper from './helper'
import { HuyenQuan } from '../database/entity/HuyenQuan.entity'

export default {
  // [GET] /xaPhuong/?page=1
  getXaPhuongs: async function (req: Request, res: Response) {
    const pageNumber = Number(req.query.page) || 1
    const pageSize = Number(req.query.pageSize) || 10
    const xaPhuongRepository = AppDataSource.getRepository(XaPhuong)
    const result = await helper.pagination(
      xaPhuongRepository,
      pageNumber,
      pageSize
    )
    res.json(result)
  },

  // [GET] /xaPhuong/:id
  getXaPhuong: async function (req: Request, res: Response) {
    const id = String(req.params.id)
    const xaPhuongRepository = AppDataSource.getRepository(XaPhuong)
    const result = await helper.getAmountDoubleJoin(
      xaPhuongRepository,
      'huyenQuan',
      'getOne',
      `alias.id = ${id}`
    )
    res.json(result)
  },

  // [POST] /xaPhuong
  postXaPhuong: async function (req: Request, res: Response) {
    const id = String(req.body.id)

    const huyenQuanRepository = AppDataSource.getRepository(HuyenQuan)
    const xaPhuongRepository = AppDataSource.getRepository(XaPhuong)

    try {
      const xaPhuong = new XaPhuong()
      const huyenQuanId = Number(req.body.huyenQuanId)
      if (huyenQuanId)
        xaPhuong.huyenQuan = await helper.checkFindOne(
          res,
          huyenQuanRepository,
          { id: huyenQuanId },
          ['huyenQuanId is invalid', 'huyenQuanId must be an integer number']
        )
      xaPhuong.ten = String(req.body.ten)
      xaPhuong.id = id

      // validate
      await helper.validate(res, xaPhuong)
      const result = await xaPhuongRepository.save(xaPhuong)
      res.json({ msg: 'Insert complete', result: JSON.stringify(result) })
    } catch (error) {}
  },

  // [PUT] /xaPhuong/:id
  putXaPhuong: async function (req: Request, res: Response) {
    const id = String(req.params.id)
    const huyenQuanRepository = AppDataSource.getRepository(HuyenQuan)
    const xaPhuongRepository = AppDataSource.getRepository(XaPhuong)

    try {
      // check valid id
      const huyenQuanId = Number(req.body.huyenQuanId)
      const xaPhuong = await helper.checkFindOne(
        res,
        xaPhuongRepository,
        { id: id },
        ['id is invalid', 'id must be a string']
      )
      if (huyenQuanId)
        xaPhuong.huyenQuan = await helper.checkFindOne(
          res,
          huyenQuanRepository,
          { id: huyenQuanId },
          ['huyenQuanId is invalid', 'huyenQuanId must be an integer number']
        )
      xaPhuong.id = id
      xaPhuong.ten = String(req.body.ten)

      // validate
      await helper.validate(res, xaPhuong)
      const result = await xaPhuongRepository.save(xaPhuong)
      res.json({ msg: 'Update complete', result: JSON.stringify(result) })
    } catch (error) {}
  },

  // [DELETE] /xaPhuong/:id
  deleteXaPhuong: async function (req: Request, res: Response) {
    const id = String(req.params.id)
    const xaPhuongRepository = AppDataSource.getRepository(XaPhuong)

    try {
      const xaPhuong = await xaPhuongRepository.findOneBy({ id: id })
      const result = await xaPhuongRepository.remove(xaPhuong)
      res.json({ msg: 'Delete complete', result: JSON.stringify(result) })
    } catch (error) {
      res.json({ msg: 'Invalid id or having foreign key' })
    }
  },
}
