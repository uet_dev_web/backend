import { Request, Response } from 'express'
import { CoSo } from '../database/entity/CoSo.entity'
import AppDataSource from '../database/connection'
import helper from './helper'
import { HuyenQuan } from '../database/entity/HuyenQuan.entity'
import { XaPhuong } from '../database/entity/XaPhuong.entity'
import { ChungNhan } from '../database/entity/ChungNhan.entity'

export default {
  // [GET] /coSo/?page=1
  getCoSos: async function (req: Request, res: Response) {
    const pageNumber = Number(req.query.page) || 1
    const pageSize = Number(req.query.pageSize) || 10
    const coSoRepository = AppDataSource.getRepository(CoSo)
    const result = await helper.paginationQuatarJoin(
      coSoRepository,
      ['huyenQuan', 'xaPhuong', 'chungNhan'],
      pageNumber,
      pageSize,
      req.session.diaBan ? `huyenQuan.id = '${req.session.diaBan}'` : ``
    )
    res.json(result)
  },

  // [GET] /coSo/:id
  getCoSo: async function (req: Request, res: Response) {
    const id = Number(req.params.id)
    const coSoRepository = AppDataSource.getRepository(CoSo)
    let where = `alias.id = ${id}`
    where += req.session.diaBan
      ? ` and huyenQuan.id = '${req.session.diaBan}'`
      : ``
    const result = await helper.getAmountQuatarJoin(
      coSoRepository,
      ['huyenQuan', 'xaPhuong', 'chungNhan'],
      'getOne',
      where
    )
    res.json(result)
  },

  // [POST] /coSo
  postCoSo: async function (req: Request, res: Response) {
    const coSoRepository = AppDataSource.getRepository(CoSo)
    const huyenQuanRepository = AppDataSource.getRepository(HuyenQuan)
    const xaPhuongRepository = AppDataSource.getRepository(XaPhuong)
    const chungNhanRepository = AppDataSource.getRepository(ChungNhan)
    const huyenQuanId = Number(req.body.huyenQuanId)
    const xaPhuongId = Number(req.body.xaPhuongId)
    const chungNhanId = Number(req.body.chungNhanId)

    try {
      const coSo = new CoSo()
      // check valid id
      coSo.huyenQuan = await helper.checkFindOne(
        res,
        huyenQuanRepository,
        { id: huyenQuanId },
        ['huyenQuanId is invalid']
      )
      coSo.xaPhuong = await helper.checkFindOne(
        res,
        xaPhuongRepository,
        { id: xaPhuongId },
        ['xaPhuongId is invalid']
      )

      // check chungNhanId
      if (chungNhanId) {
        const chungNhan = await helper.getAmountDoubleJoin(
          chungNhanRepository,
          'coSo',
          'getOne',
          `alias.id = ${chungNhanId}`
        )

        // if chungNhanId is valid
        if (!chungNhan) {
          res.json({
            msg: 'Insert fail',
            error: [
              'chungNhanId is invalid',
              'chungNhanId must be an integer number',
            ],
          })
          throw new Error('chungNhanId invalid')
        }

        // check available, if not check, request timeout
        if (chungNhan.coSo) {
          res.json({
            msg: 'Insert fail',
            error: ['chungNhanId is unavailable'],
          })
          throw new Error('chungNhanId is unavailable')
        } else coSo.chungNhan = chungNhan
      }
      coSo.ten = String(req.body.ten)
      coSo.diaChi = String(req.body.diaChi)
      coSo.loaiHinh = String(req.body.loaiHinh)
      coSo.sdt = String(req.body.sdt)

      // validate
      await helper.validate(res, coSo)
      const result = await coSoRepository.save(coSo)
      res.json({ msg: 'Insert complete', result: JSON.stringify(result) })
    } catch (error) {}
  },

  // [PUT] /coSo/:id
  putCoSo: async function (req: Request, res: Response) {
    const id = Number(req.params.id)
    const coSoRepository = AppDataSource.getRepository(CoSo)
    const huyenQuanRepository = AppDataSource.getRepository(HuyenQuan)
    const xaPhuongRepository = AppDataSource.getRepository(XaPhuong)
    const chungNhanRepository = AppDataSource.getRepository(ChungNhan)
    const huyenQuanId = Number(req.body.huyenQuanId)
    const xaPhuongId = Number(req.body.xaPhuongId)
    const chungNhanId = Number(req.body.chungNhanId)

    try {
      // check valid id
      const coSo = await helper.checkFindOne(res, coSoRepository, { id: id }, [
        'id is invalid',
        'id must be an integer number',
      ])
      coSo.huyenQuan = await helper.checkFindOne(
        res,
        huyenQuanRepository,
        { id: huyenQuanId },
        ['huyenQuanId is invalid']
      )
      coSo.xaPhuong = await helper.checkFindOne(
        res,
        xaPhuongRepository,
        { id: xaPhuongId },
        ['xaPhuongId is invalid']
      )
      // check chungNhanId
      if (chungNhanId) {
        const chungNhan = await helper.getAmountDoubleJoin(
          chungNhanRepository,
          'coSo',
          'getOne',
          `alias.id = ${chungNhanId}`
        )

        // if chungNhanId is valid
        if (!chungNhan) {
          res.json({
            msg: 'Update fail',
            error: [
              'chungNhanId is invalid',
              'chungNhanId must be an integer number',
            ],
          })
          throw new Error('chungNhanId invalid')
        }

        // check available, if not check, request timeout
        if (chungNhan.coSo) {
          res.json({
            msg: 'Update fail',
            error: ['chungNhanId is unavailable'],
          })
          throw new Error('chungNhanId is unavailable')
        } else coSo.chungNhan = chungNhan
      }
      coSo.ten = String(req.body.ten)
      coSo.diaChi = String(req.body.diaChi)
      coSo.loaiHinh = String(req.body.loaiHinh)
      coSo.sdt = String(req.body.sdt)

      // validate
      await helper.validate(res, coSo)
      const result = await coSoRepository.save(coSo)
      res.json({ msg: 'Update complete', result: JSON.stringify(result) })
    } catch (error) {}
  },

  // [DELETE] /coSo/:id
  deleteCoSo: async function (req: Request, res: Response) {
    const id = Number(req.params.id)
    const coSoRepository = AppDataSource.getRepository(CoSo)

    try {
      const coSo = await coSoRepository.findOneBy({ id: id })
      const result = await coSoRepository.remove(coSo)
      res.json({ msg: 'Delete complete', result: JSON.stringify(result) })
    } catch (error) {
      res.json({ msg: 'Invalid id or having foreign key' })
    }
  },
}
