import { Request, Response } from 'express'
import { NguoiDung } from '../database/entity/NguoiDung.entity'
import AppDataSource from '../database/connection'
import helper from './helper'
import { helper as authHelper } from '../middlewares/auth'

export default {
  login: async (req: Request, res: Response) => {
    const username = req.body.username
    const password = req.body.password

    // check null
    if (!username || !password) {
      res.json({ msg: 'Username or password incorrect' })
      return
    }

    // check username password in database
    const userRepository = AppDataSource.getRepository(NguoiDung)
    const findUser = await userRepository.findOneBy({ username, password })
    let result = { msg: 'Username or password incorrect' }

    // if user valid
    if (findUser) {
      const user = await helper.getAmountDoubleJoin(
        userRepository,
        'diaBan',
        'getOne',
        `username = '${username}' and password = '${password}'`
      )
      req.session.username = user.username
      req.session.diaBan = user.diaBan ? user.diaBan.id : ''

      const filtered = helper.filterPropUser(user)
      authHelper.setToken(req, filtered)
      result.msg = 'Login success'
      result = Object.assign({}, result, { data: filtered })
    }
    res.json(result)
  },

  logout: (req: Request, res: Response) => {
    req.session.destroy((err) => {
      if (err) console.log('Logout-error: ', err)
    })
    res.json({ msg: 'Lougout success' })
  },
}
