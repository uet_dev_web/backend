import { Request, Response } from 'express'
import { ChungNhan } from '../database/entity/ChungNhan.entity'
import AppDataSource from '../database/connection'
import helper from './helper'
import { CoSo } from '../database/entity/CoSo.entity'

export default {
  // [GET] /chungNhan/?page=1
  getChungNhans: async function (req: Request, res: Response) {
    const pageNumber = Number(req.query.page) || 1
    const pageSize = Number(req.query.pageSize) || 10
    const chungNhanRepository = AppDataSource.getRepository(ChungNhan)
    const result = await helper.paginationTripleJoinVer2(
      chungNhanRepository,
      [
        { type: 'leftJoinAndSelect', key: 'alias.coSo', value: 'coSo' },
        { type: 'leftJoin', key: 'coSo.huyenQuan', value: 'huyenQuan' },
      ],
      pageNumber,
      pageSize,
      req.session.diaBan ? `huyenQuan.id = '${req.session.diaBan}'` : ``
    )
    res.json(result)
  },

  // [GET] /chungNhan/:id
  getChungNhan: async function (req: Request, res: Response) {
    const id = Number(req.params.id)
    const chungNhanRepository = AppDataSource.getRepository(ChungNhan)
    let where = `alias.id = ${id}`
    where += req.session.diaBan
      ? ` and huyenQuan.id = '${req.session.diaBan}'`
      : ``
    const result = await helper.getAmountTripleJoinVer2(
      chungNhanRepository,
      [
        { type: 'leftJoinAndSelect', key: 'alias.coSo', value: 'coSo' },
        { type: 'leftJoin', key: 'coSo.huyenQuan', value: 'huyenQuan' },
      ],
      'getOne',
      where
    )
    res.json(result)
  },

  // [POST] /chungNhan
  postChungNhan: async function (req: Request, res: Response) {
    const chungNhanRepository = AppDataSource.getRepository(ChungNhan)
    const coSoRepository = AppDataSource.getRepository(CoSo)
    const coSoId = Number(req.body.coSoId)

    try {
      const chungNhan = new ChungNhan()
      // check valid id
      if (coSoId)
        chungNhan.coSo = await helper.checkFindOne(
          res,
          coSoRepository,
          { id: coSoId },
          ['coSoId is invalid', 'coSoId must be an integer number']
        )
      chungNhan.hieuLuc = Boolean(req.body.hieuLuc)
      const ngayHetHan = String(req.body.ngayHetHan).split('-')
      chungNhan.ngayHetHan = new Date(
        Number(ngayHetHan[0]),
        Number(ngayHetHan[1]),
        Number(ngayHetHan[2])
      )
      chungNhan.ngayCap = new Date()

      // validate
      await helper.validate(res, chungNhan)
      const result = await chungNhanRepository.save(chungNhan)
      res.json({ msg: 'Insert complete', result: JSON.stringify(result) })
    } catch (error) {}
  },

  // [PUT] /chungNhan/:id
  putChungNhan: async function (req: Request, res: Response) {
    const id = Number(req.params.id)
    const chungNhanRepository = AppDataSource.getRepository(ChungNhan)
    const coSoRepository = AppDataSource.getRepository(CoSo)
    const coSoId = Number(req.body.coSoId)

    try {
      // check valid id
      const chungNhan = await helper.checkFindOne(
        res,
        chungNhanRepository,
        { id: id },
        ['id is invalid', 'id must be an integer number']
      )
      if (coSoId)
        chungNhan.coSo = await helper.checkFindOne(
          res,
          coSoRepository,
          { id: coSoId },
          ['coSoId is invalid', 'coSoId must be an integer number']
        )
      chungNhan.hieuLuc = Boolean(req.body.hieuLuc)
      const ngayHetHan = String(req.body.ngayHetHan).split('-')
      chungNhan.ngayHetHan = new Date(
        Number(ngayHetHan[0]),
        Number(ngayHetHan[1]),
        Number(ngayHetHan[2])
      )

      // validate
      await helper.validate(res, chungNhan)
      const result = await chungNhanRepository.save(chungNhan)
      res.json({ msg: 'Update complete', result: JSON.stringify(result) })
    } catch (error) {}
  },

  // [DELETE] /chungNhan/:id
  deleteChungNhan: async function (req: Request, res: Response) {
    const id = Number(req.params.id)
    const chungNhanRepository = AppDataSource.getRepository(ChungNhan)

    try {
      const chungNhan = await chungNhanRepository.findOneBy({ id: id })
      const result = await chungNhanRepository.remove(chungNhan)
      res.json({ msg: 'Delete complete', result: JSON.stringify(result) })
    } catch (error) {
      res.json({ msg: 'Invalid id' })
    }
  },
}
