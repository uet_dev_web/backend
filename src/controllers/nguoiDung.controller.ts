import { Request, Response } from 'express'
import { NguoiDung } from '../database/entity/NguoiDung.entity'
import AppDataSource from '../database/connection'
import helper from './helper'
import { HuyenQuan } from '../database/entity/HuyenQuan.entity'

export default {
  // [GET] /nguoiDung?page=1
  getNguoiDungs: async function (req: Request, res: Response) {
    const pageNumber = Number(req.query.page) || 1
    const pageSize = Number(req.query.pageSize) || 10
    const userRepository = AppDataSource.getRepository(NguoiDung)
    const result = await helper.paginationDoubleJoin(
      userRepository,
      'diaBan',
      pageNumber,
      pageSize
    )

    // // filter user
    // const data = []
    // result.data.forEach((user) => {
    //   data.push(helper.filterPropUser(user))
    // })
    // result.data = data
    res.json(result)
  },

  // [GET] /nguoiDung/:username
  getNguoiDung: async function (req: Request, res: Response) {
    const username = req.params.username
    const nguoiDungRepository = AppDataSource.getRepository(NguoiDung)
    const result = await helper.getAmountDoubleJoin(
      nguoiDungRepository,
      'diaBan',
      'getOne',
      `alias.username = '${username}'`
    )
    res.json(result)
  },

  // [GET] /nguoiDung/info
  getSelfInfo: async function (req: Request, res: Response) {
    const username = req.session.username
    const nguoiDungRepository = AppDataSource.getRepository(NguoiDung)
    const result = await helper.getAmountDoubleJoin(
      nguoiDungRepository,
      'diaBan',
      'getOne',
      `alias.username = '${username}'`
    )
    res.json(result)
  },

  // [POST] /nguoiDung
  postNguoiDung: async function (req: Request, res: Response) {
    const nguoiDungRepository = AppDataSource.getRepository(NguoiDung)
    const diaBanRepository = AppDataSource.getRepository(HuyenQuan)

    try {
      const nguoiDung = new NguoiDung()
      const diaBanId = String(req.body.diaBanId || '')
      console.log(diaBanId)
      if (diaBanId)
        nguoiDung.diaBan = await helper.checkFindOne(
          res,
          diaBanRepository,
          { id: diaBanId },
          ['diaBanId is invalid', 'diaBanId must be a string']
        )
      nguoiDung.username = String(req.body.username)
      nguoiDung.password = String(req.body.password)

      // validate
      await helper.validate(res, nguoiDung)
      const result = await nguoiDungRepository.save(nguoiDung)
      res.json({ msg: 'Insert complete', result: JSON.stringify(result) })
    } catch (error) {}
  },

  // [PUT] /nguoiDung/:username
  putNguoiDung: async function (req: Request, res: Response) {
    const username = String(req.params.username)
    const nguoiDungRepository = AppDataSource.getRepository(NguoiDung)
    const diaBanRepository = AppDataSource.getRepository(HuyenQuan)

    try {
      // check valid id
      const diaBanId = String(req.body.diaBanId || '')
      const nguoiDung = await helper.checkFindOne(
        res,
        nguoiDungRepository,
        { username: username },
        ['username is invalid', 'username must be a string']
      )
      if (diaBanId)
        nguoiDung.diaBan = await helper.checkFindOne(
          res,
          diaBanRepository,
          { id: diaBanId },
          ['diaBanId is invalid', 'diaBanId must be be a string']
        )

      nguoiDung.password = String(req.body.password)

      // validate
      await helper.validate(res, nguoiDung)
      const result = await nguoiDungRepository.save(nguoiDung)
      res.json({ msg: 'Update complete', result: JSON.stringify(result) })
    } catch (error) {}
  },

  // [DELETE] /nguoiDung/:username
  deleteNguoiDung: async function (req: Request, res: Response) {
    const username = String(req.params.username)
    const nguoiDungRepository = AppDataSource.getRepository(NguoiDung)

    try {
      const nguoiDung = await nguoiDungRepository.findOneBy({
        username: username,
      })
      const result = await nguoiDungRepository.remove(nguoiDung)
      res.json({ msg: 'Delete complete', result: JSON.stringify(result) })
    } catch (error) {
      res.json({ msg: 'Invalid id' })
    }
  },
}
