import { Request, Response } from 'express'
import path = require('path')

export default {
  // [GET] /
  getSwaggerFile: async function (req: Request, res: Response) {
    res.redirect('/swagger')
  },

  // [GET] /
  catch404: async function (req: Request, res: Response) {
    res.json({ msg: 'Page not found' })
  },
}
