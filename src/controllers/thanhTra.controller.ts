import { Request, Response } from 'express'
import { ThanhTra } from '../database/entity/ThanhTra.entity'
import AppDataSource from '../database/connection'
import helper from './helper'
import { CoSo } from '../database/entity/CoSo.entity'

export default {
  // [GET] /thanhTra/?page=1
  getThanhTras: async function (req: Request, res: Response) {
    const pageNumber = Number(req.query.page) || 1
    const pageSize = Number(req.query.pageSize) || 10
    const thanhTraRepository = AppDataSource.getRepository(ThanhTra)
    const result = await helper.paginationQuatarJoinVer2(
      thanhTraRepository,
      [
        { type: 'leftJoinAndSelect', key: 'alias.coSo', value: 'coSo' },
        { type: 'leftJoin', key: 'coSo.huyenQuan', value: 'huyenQuan' },
        { type: 'leftJoinAndSelect', key: 'alias.mauVats', value: 'mauVats' },
      ],
      pageNumber,
      pageSize,
      req.session.diaBan ? `huyenQuan.id = '${req.session.diaBan}'` : ``
    )
    res.json(result)
  },

  // [GET] /thanhTra/:id
  getThanhTra: async function (req: Request, res: Response) {
    const id = Number(req.params.id)
    const thanhTraRepository = AppDataSource.getRepository(ThanhTra)
    let where = `alias.id = ${id}`
    where += req.session.diaBan
      ? ` and huyenQuan.id = '${req.session.diaBan}'`
      : ``
    const result = await helper.getAmountQuatarJoinVer2(
      thanhTraRepository,
      [
        { type: 'leftJoinAndSelect', key: 'alias.coSo', value: 'coSo' },
        { type: 'leftJoin', key: 'coSo.huyenQuan', value: 'huyenQuan' },
        { type: 'leftJoinAndSelect', key: 'alias.mauVats', value: 'mauVats' },
      ],
      'getOne',
      where
    )
    res.json(result)
  },

  // [POST] /thanhTra
  postThanhTra: async function (req: Request, res: Response) {
    const id = String(req.body.id)
    const coSoRepository = AppDataSource.getRepository(CoSo)
    const thanhTraRepository = AppDataSource.getRepository(ThanhTra)

    try {
      const thanhTra = new ThanhTra()
      const coSoId = Number(req.body.coSoId)
      if (coSoId)
        thanhTra.coSo = await helper.checkFindOne(
          res,
          coSoRepository,
          { id: coSoId },
          ['coSoId is invalid', 'coSoId must be an integer number']
        )
      thanhTra.ngayBatDau = new Date()
      thanhTra.ngayKetThuc = new Date()

      // validate
      await helper.validate(res, thanhTra)
      const result = await thanhTraRepository.save(thanhTra)
      res.json({ msg: 'Insert complete', result: JSON.stringify(result) })
    } catch (error) {}
  },

  // [PUT] /thanhTra/:id
  putThanhTra: async function (req: Request, res: Response) {
    const id = Number(req.params.id)
    const coSoRepository = AppDataSource.getRepository(CoSo)
    const thanhTraRepository = AppDataSource.getRepository(ThanhTra)

    try {
      // check valid id
      const coSoId = Number(req.body.coSoId)
      const thanhTra = await helper.checkFindOne(
        res,
        thanhTraRepository,
        { id: id },
        ['id is invalid', 'id must be a string']
      )
      if (coSoId)
        thanhTra.coSo = await helper.checkFindOne(
          res,
          coSoRepository,
          { id: coSoId },
          ['coSoId is invalid', 'coSoId must be an integer number']
        )

      thanhTra.ngayBatDau = new Date()
      thanhTra.ngayKetThuc = new Date()

      // validate
      await helper.validate(res, thanhTra)
      const result = await thanhTraRepository.save(thanhTra)
      res.json({ msg: 'Update complete', result: JSON.stringify(result) })
    } catch (error) {}
  },

  // [DELETE] /thanhTra/:id
  deleteThanhTra: async function (req: Request, res: Response) {
    const id = Number(req.params.id)
    const thanhTraRepository = AppDataSource.getRepository(ThanhTra)

    try {
      const thanhTra = await thanhTraRepository.findOneBy({ id: id })
      const result = await thanhTraRepository.remove(thanhTra)
      res.json({ msg: 'Delete complete', result: JSON.stringify(result) })
    } catch (error) {
      res.json({ msg: 'Invalid id or having foreign key' })
    }
  },
}
