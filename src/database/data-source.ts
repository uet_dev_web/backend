require('dotenv').config()
import 'reflect-metadata'
import { DataSource } from 'typeorm'

export const AppDataSource = new DataSource({
  type: 'mysql',
  host: process.env.DATABASE_HOST,
  port: parseInt(process.env.DATABASE_PORT),
  username: process.env.DATABASE_USERNAME,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE_NAME,
  timezone: process.env.TIMEZONE,
  synchronize: true,
  entities: [__dirname + '/entity/*.ts', __dirname + '/entity/**/*.ts'],
  migrations: [],
  subscribers: [],
})
