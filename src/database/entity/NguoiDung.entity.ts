import { Column, Entity, ManyToOne, OneToOne, PrimaryColumn } from 'typeorm'
import { IsInt, IsString, Min, IsBoolean } from 'class-validator'
import { HuyenQuan } from './HuyenQuan.entity'

@Entity()
export class NguoiDung {
  @PrimaryColumn({ type: 'varchar', length: 255 })
  @IsString()
  username: string

  @Column({ type: 'varchar', length: 255 })
  @IsString()
  password: string

  @ManyToOne((type) => HuyenQuan, (huyenQuan) => huyenQuan.id)
  diaBan: HuyenQuan
}
