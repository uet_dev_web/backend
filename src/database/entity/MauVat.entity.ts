import { IsInt, IsString, Min, IsBoolean } from 'class-validator'
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { ThanhTra } from './ThanhTra.entity'

@Entity()
export class MauVat {
  @PrimaryGeneratedColumn()
  id: number

  @ManyToOne((type) => ThanhTra, (thanhTra) => thanhTra.mauVats)
  thanhTra: ThanhTra

  @Column({ type: 'boolean', default: false })
  @IsBoolean()
  anToan: boolean
}
