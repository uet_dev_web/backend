import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BeforeInsert,
  AfterUpdate,
  ManyToOne,
  OneToMany,
  IsNull,
  OneToOne,
  JoinColumn,
} from 'typeorm'

import {
  IsBoolean,
  IsDate,
  IsInt,
  IsNumber,
  IsString,
  IsUrl,
  Min,
} from 'class-validator'
import { ChungNhan } from './ChungNhan.entity'
import { ThanhTra } from './ThanhTra.entity'
import { HuyenQuan } from './HuyenQuan.entity'
import { XaPhuong } from './XaPhuong.entity'

@Entity()
export class CoSo {
  @PrimaryGeneratedColumn({ type: 'int', unsigned: true })
  id: number

  @Column({ type: 'varchar', length: 255 })
  @IsString()
  ten: string

  @Column({ type: 'text', nullable: true })
  @IsString()
  diaChi: string

  @Column({ type: 'varchar', length: 20, nullable: true })
  @IsString()
  sdt: string

  @Column({ type: 'varchar', length: 255, nullable: true })
  @IsString()
  loaiHinh: string

  @ManyToOne((type) => HuyenQuan, (huyenQuan) => huyenQuan.coSos)
  huyenQuan: HuyenQuan

  @ManyToOne((type) => XaPhuong, (xaPhuong) => xaPhuong.coSos)
  xaPhuong: XaPhuong

  @OneToOne((type) => ChungNhan, (chungNhan) => chungNhan.coSo)
  @JoinColumn()
  chungNhan: ChungNhan

  @OneToMany((type) => ThanhTra, (thanhTra) => thanhTra.coSo)
  thanhTras: ThanhTra[]
}
