import { IsString } from 'class-validator'
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BeforeInsert,
  AfterUpdate,
  ManyToOne,
  IsNull,
  OneToMany,
  PrimaryColumn,
} from 'typeorm'
import { CoSo } from './CoSo.entity'
import { NguoiDung } from './NguoiDung.entity'

import { XaPhuong } from './XaPhuong.entity'

@Entity()
export class HuyenQuan {
  @PrimaryColumn()
  @IsString()
  id: string

  @Column({ type: 'varchar', length: 255 })
  @IsString()
  ten: string

  @OneToMany((type) => XaPhuong, (xaPhuong) => xaPhuong.huyenQuan)
  xaPhuongs: XaPhuong[]

  @OneToMany((type) => CoSo, (coSo) => coSo.huyenQuan)
  coSos: CoSo[]

  @OneToMany((type) => NguoiDung, (nguoiDung) => nguoiDung.diaBan)
  nguoiDungs: NguoiDung[]
}
