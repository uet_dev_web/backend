import { IsInt, IsString, Min, IsBoolean, IsUrl, IsDate } from 'class-validator'
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { CoSo } from './CoSo.entity'
import { MauVat } from './MauVat.entity'

@Entity()
export class ThanhTra {
  @PrimaryGeneratedColumn()
  id: number

  @Column({ type: 'datetime', nullable: true })
  @IsDate()
  ngayBatDau: Date

  @Column({ type: 'datetime', nullable: true })
  @IsDate()
  ngayKetThuc: Date

  @ManyToOne((type) => CoSo, (coSo) => coSo.thanhTras)
  coSo: CoSo

  @OneToMany((type) => MauVat, (mauVat) => mauVat.thanhTra)
  mauVats: MauVat[]
}
