import { IsInt, IsString, Min, IsBoolean, IsDate } from 'class-validator'
import {
  Column,
  Entity,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { CoSo } from './CoSo.entity'

@Entity()
export class ChungNhan {
  @PrimaryGeneratedColumn({ type: 'int', unsigned: true })
  id: number

  @Column({ type: 'datetime', nullable: true })
  @IsDate()
  ngayCap: Date

  @Column({ type: 'datetime' })
  @IsDate()
  ngayHetHan: Date

  @Column({ type: 'boolean', default: false })
  @IsBoolean()
  hieuLuc: boolean

  @OneToOne((type) => CoSo, (coSo) => coSo.chungNhan)
  coSo: CoSo
}
