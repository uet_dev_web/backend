import { IsString } from 'class-validator'
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  BeforeInsert,
  AfterUpdate,
  ManyToOne,
  OneToOne,
  OneToMany,
  PrimaryColumn,
} from 'typeorm'
import { CoSo } from './CoSo.entity'
import { HuyenQuan } from './HuyenQuan.entity'

@Entity()
export class XaPhuong {
  @PrimaryColumn()
  @IsString()
  id: string

  @Column({ type: 'varchar', length: 255 })
  @IsString()
  ten: string

  @ManyToOne((type) => HuyenQuan, (huyenQuan) => huyenQuan.xaPhuongs)
  huyenQuan: HuyenQuan

  @OneToMany((type) => CoSo, (coSo) => coSo.xaPhuong)
  coSos: CoSo[]
}
